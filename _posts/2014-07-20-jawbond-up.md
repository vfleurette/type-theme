---
layout: post
title: Jawbone UP
feature-img: "img/2014-07-20-jawbone-up/cover.jpg"
---



Hello everybody, today I will present you a revolutionary wristband : the Jawbone Up. This digital wristband dubbed intelligent, allows the person wearing it   to mesure the different activities tied to the weel being of the wearer. We call it : the quantified self. At the moment, we count many connected wristband like the FuelBand from Nike or the Flex from Fitbit. 

{% highlight ruby %}Trendy, flexible and waterproof, this wristband is very discreet and so very easy to wear.{% endhighlight %}

You will tell me that this is really interesting but what is the purpose of this ? This wristband has many functions. First, you need to know that you are supposed to wear this wristband at any moment of the day that’s why there are two differents modes : the day mode and the sleep mode. This changement of mode is done by simple pressure on the only one button of the wristband.
The day, its main function is the pedometer functionality. Everyday, you should reach your target number of steps and be careful if it isn’t reached … It also allows you to have a follow up of your sports activities. You can note that it is very accurate regarding the calorie expenditure. For example, during a ping-pong match of fifteen minutes your health comrade considers that you expend five hundred calories. And, admire this cyclist climbing a very long and hard hill ! According to Jawbone Up despite his morning run, he is supposed to have expended only one hundred calories. However, we recommend you put the wristband on the foot when you go cycling. But, be careful, not to put the wristband on your foot when you are playing ping-pong. This virtual handler has also an inactivity alarm. 

![jawbone-up/]({{ url }}/img/2014-07-20-jawbone-up/jawbone-up.jpg)


The wristband Jawbone Up dispose of a sleep mode. It calculates the duration of your sleep in case you don’t sleep for a long time to motivate you for your big working day. It also announces the time you spend to fall asleep : it confirms you in fact you spend two hours to sleep. Yes .. this wristband is indispensable ! It gives you the number of times when you wake up during the night if you don’t know it and were sleeping. Finally, in this sleep mode an unknown algorithm lets you know the duration of your deep and light sleep. One revolutionary functionality is that it serves as an alarm clock : when it considers that you are in the end of your sleeping cycle, in one timming interval you gave, it wakes you up by vibrating … well it tries !

Finally, the data synchronization is realized using a mobile phone thanks to the application : Up. This application allows you to analyse your sleep and see your activity during the day. This object makes you more addicted than you are now about your mobile phone because without it, no visual allows you to exploit the functionalities of the wristband.
To conclude this kind of technology booming is devoted to developed and particularly to make better. This kind of gadget allows users, upon condition of minimal involvement, keep a vital balance or justify of    activities or otherwise of its disable. As a lot of doctors say, depressions and uneasiness is often binded to much frail or short sleep: the jawbone UP try to re-establish this balance.
However the question shall nevertheless remain in abeyance is do we really  need one electronic object to motivate us to move. 

**Don’t we become dependant from news technologies for common activities ?**
