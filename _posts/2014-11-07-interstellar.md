---
layout: post
title: Interstellar
feature-img: "img/interstellar.jpg"
---
Je viens de voir Interstellar. Et j'ai vraiment beaucoup aimé. C'est à mi-chemin entre de la science-fiction et une réflexion philosophique sur le chemin parcouru par l'Humanité. Mais ayant lu beaucoup de SF, il y a toujours un fond philosophique... Donc à mon sens, c'est un film de SF à 100%. Ce que Interstellar nous dit c'est que nos vies bien que tangibles ne sont que de drôles de rêves qui obéissent aux lois de la physique et qu'il suffirait d'un rien pour apprivoiser ces lois. Le temps
et l'espace sont relatifs et intimement liés. Tout comme notre existence et notre mort. Toutefois Interstellar place l'Humain au centre de tout, ce qui est, à certains moments très arrogant.
