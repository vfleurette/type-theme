---
layout: post
title: Nouvelle année 2015
feature-img: "img/new_year.jpg"
---

Le site de la iServ.fr a fêté ses 1 an avec ma totale indifférence, en fait j’ai tout simplement oublié la date de création…
Ce n’était pas possible de laisser cet oubli, du coup j’ai pris mon petit éditeur de texte favori (*vim*)et j’ai quelque peu modifié
le thème, dont vous voyez le résultat… Si la mode est au “flat design”, pour moi ce sera du “white design”.

Ainsi, pas de grosses folies juste une énième simplification du site :

- Exit la page d’archive très peu consultée,
- Ouste la page des articles trop complexe à maintenir (selon mes critères),
- Au revoir barre de navigation

Et en ce qui concerne les ajouts/retours :

- Le bouton Twitter devrai  son retour,
- Amélioration de la zone de lecture qui était trop large

Pour l’instant Google Analytics est encore la solution utilisée parce que je n’ai pas encore osé installer Piwik, mais clairement c’est un de mes objectifs à un peu plus long terme.

Voilà, j’espère que vous serez encore plus nombreux à venir lire les articles.