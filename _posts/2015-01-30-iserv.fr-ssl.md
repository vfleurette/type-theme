---
layout: post
title: iServ en SSL
feature-img: "img/https.jpg"
---
Le site iServ.fr est accessible de manière sécurisé avec le protocole **HTTPS**. Ce n’est pas parce que je suis un féru de sécurité
mais parce que j’avais envie d’apprendre.

C’est chose faite depuis cette nuit grâce à un certificat gratuit fourni par StartSSL.com, toutes les personnes souhaitant accéder à
*http://iserv.fr* seront automatiquement redirigés vers *https://iserv.fr*. J'essayerai d'en faire un article.

Concrètement, cela ne change rien pour les visiteurs, ils seront juste informés qu’ils se connectent à un site avec une “Identité
validée”.

J’ai bien préparé le changement avec de nombreux tests sur toutes les plateformes, mais comme toujours il peut y avoir des petits
soucis. Du coup, je suis preneur de vos retours au moindre petit soucis.