/* MAIN */
  Develop & Main Design: Vincent Fleurette
  Twitter: @vincfleurette
  Site: https://iserv.fr

/* SITE */
  Last update: 25/08/2014
  Doctype: HTML5
  developement tools: VimR, Jekyll, GIT, Pixelmator,

/* SPECIAL THANKS */
  Designer: Johan BLEUZEN 
  Twitter: @jbleuzen
 

  And, of course, all humanstxt.org team!